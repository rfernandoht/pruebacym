import { Link } from "react-router-dom";

const navBar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">rfernandoht - Prueba C&M</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-Link" to="/">Ver Comics</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-Link" to="/comicFavoritosList/2">Ver Comics Favoritos</Link>
                        </li>
                    </ul>
                    <ul>
                        <li className="nav-item">
                            <Link className="nav-Link" to="/usuarioForm">Editar perfil</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
};

export default navBar;