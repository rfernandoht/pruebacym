const API_URL = "http://localhost:8000/api/comics/";
const API_URL_FAV = "http://localhost:8000/api/comicsfavoritos/";

//Comic

export const listComics = async () => {
    return await fetch(API_URL,);
}

export const getComic = async (comicId) => {
    return await fetch(`${API_URL}${comicId}`, {
        method: 'GET'
    });
}

export const deleteComic = async (comicId) => {
    //console.log(newComic.id);
    return await fetch(`${API_URL}${comicId}`, {
        method: 'DELETE'
    });
};

//Comic Favoritos

export const listComicsFav = async (usuaId) => {
    return await fetch(`${API_URL_FAV}${usuaId}`,);
}

export const registraComicFavorito = async (usuaId, comiId) => {
    //console.log(usuaId, comiId);
    return await fetch(API_URL_FAV, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "usua": usuaId,
            "comi": comiId,
        })
    });
};

export const deleteComicFavorito = async (usuaId, comiId) => {
    return await fetch(`${API_URL_FAV}${usuaId}/${comiId}`, {
        method: 'DELETE'
    });
};