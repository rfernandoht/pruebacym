import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import * as ComicServer from "./ComicServer"

const ComicItemDetalle = () => {
    //console.log(props.comic);
    //console.log(comic);

    const params = useParams();
    //console.log(params);

    const initialState = { id: 0, nombre: "", fecha_pub: "fecha_pub", escritor: "", dibujante: "", artista_portada: "", descripcion: "", imagen: "" };

    const [comic, setComic] = useState([initialState]);

    let tmpImg = '';
    let img = '';

    const getComic = async (comicId) => {
        try {
            const res = await ComicServer.getComic(comicId);
            const data = await res.json();
            console.log("Data: ",data);
            const { nombre, fecha_pub, escritor, dibujante, artista_portada, descripcion, imagen } = data.comic;
            setComic({ nombre, fecha_pub, escritor, dibujante, artista_portada, descripcion, imagen });
            tmpImg = data.comic.imagen;
            tmpImg = tmpImg.replace("frontend/src/", "");
            img = require('../../' + tmpImg);
            console.log(img);
            
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        if (params.id) {
            getComic(params.id);
        }
        // eslint-disable-next-line
    }, []);

    return (
        <div className="col-md-3 mx-auto">
            <h2 className="mb-3 text-center">Detalle del Comic</h2>
            <div className="mb-3">
                <img src={img} alt={img} />
            </div>
            <div className="mb-3">
                <h2>{comic.nombre}</h2>
            </div>
            <div className="mb-3">
                <h3>Fecha de publicación: </h3>{comic.fecha_pub}
            </div>
            <div className="mb-3">
                <h3>Escritor: </h3>{comic.escritor}
            </div>
            <div className="mb-3">
                <h3>Dibujante: </h3>{comic.dibujante}
            </div>
            <div className="mb-3">
                <h3>Artista de portada: </h3>{comic.artista_portada}
            </div>
            <div className="mb-3">
                <h3>descripcion: </h3>{comic.descripcion}
            </div>
        </div>
    )
};

export default ComicItemDetalle;