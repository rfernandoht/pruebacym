import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

//Componentes
import ComicFavoritoItem from "./ComicFavoritoItem";

import * as ComicServer from './ComicServer';

const ComicFavoritosList = (usuaId) => {
    const [comicsFav, setComicsFav] = useState([]);

    const history = useNavigate();

    const listComicsFav = async (usuaId) => {
        try {
            const res = await ComicServer.listComicsFav(usuaId);
            const data = await res.json();
            console.log("datos: ",data)
            if (data.message === "Comics Favoritos no encontrados"){
                alert("El usuario no tiene Comics Favoritos registrados");
                history("/");
            } else {
                setComicsFav(data.comicsFav);
            }
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        listComicsFav(2);
    }, []);

    return (
        <div className="row">
            {comicsFav.map((comicFav) => (
                <ComicFavoritoItem key={comicFav.id} comicFav={comicFav} listComicsFav={listComicsFav}/>
            ))}
        </div>
    )
}

export default ComicFavoritosList;