import React, { useEffect, useState } from "react";

//Componentes
import ComicItem from "./ComicItem";

import * as ComicServer from './ComicServer';

const ComicList = () => {
    const [comics, setComics] = useState([]);

    const listComics = async () => {
        try {
            const res = await ComicServer.listComics();
            const data = await res.json();
            setComics(data.comics);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        listComics();
    }, []);

    return (
        <div className="row">
            {comics.map((comic) => (
                <ComicItem key={comic.id} comic={comic} listComics={listComics}/>
            ))}
        </div>
    )
}

export default ComicList;