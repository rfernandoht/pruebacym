import React from "react";
import { useNavigate } from "react-router-dom";

import * as ComicServer from "./ComicServer"

const ComicFavoritoItem = ({ comicFav, listComicsFav }) => {
    //console.log(props.comic);
    console.log("comicFav: ", comicFav);

    const history = useNavigate();

    const handleDeleteFavorito = async (usua_Id, comiId) => {
        console.log("Id:", usua_Id, comiId);
        await ComicServer.deleteComicFavorito(usua_Id, comiId);
        listComicsFav(usua_Id);
    }

    return (
        <div className="col-md-4 mb-4 ">
            <div className="card card-body">
                <h3 className="card-tittle">{comicFav.nombre}</h3>
                <img src={comicFav.imagen} alt={comicFav.imagen} />
                <button onClick={() => history(`/detalleComic/${comicFav.comi_id}`)} className="btn btn-primary my-2">Ver detalle</button>
                <button onClick={() => comicFav.id && handleDeleteFavorito(comicFav.usua_id, comicFav.comi_id)} className="btn btn-danger my-2">Borrar Favorito</button>
            </div>
        </div>
    )
};

export default ComicFavoritoItem;