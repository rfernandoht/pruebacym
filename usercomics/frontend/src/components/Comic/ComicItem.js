import React from "react";
import { useNavigate } from "react-router-dom";

import * as ComicServer from "./ComicServer"

const ComicItem = ({ comic, listComics }) => {
    //console.log(props.comic);
    //console.log(comic);

    const history = useNavigate();

    let tmpImg = comic.imagen;
    tmpImg = tmpImg.replace("frontend/src/", "");
    const img = require('../../' + tmpImg);

    const handleDelete = async (comicId) => {
        //console.log(comicId);
        await ComicServer.deleteComic(comicId);
        listComics();
    }

    const handleComicFavorito = async (usuaId, comiId) => {
        await ComicServer.registraComicFavorito(usuaId, comiId);
        listComics();
    }

    return (
        <div className="col-md-4 mb-4 ">
            <div className="card card-body">
                <h3 className="card-tittle">{comic.nombre}</h3>
                <img src={img} alt={img} width="50%" height="50%" />
                <button onClick={() => history(`/updateComic/${comic.id}`)} className="ms-2 btn btn-sm btn-info my-2">Actualizar</button>
                <button onClick={() => comic.id && handleComicFavorito(2, comic.id)} className="ms-2 btn btn-sm btn-info my-2">Favorito</button>
                <button onClick={() => history(`/detalleComic/${comic.id}`)} className="btn btn-primary my-2">Ver detalle</button>
                <button onClick={() => comic.id && handleDelete(comic.id)} className="btn btn-danger my-2">Eliminar Comic</button>
            </div>
        </div>
    )
};

export default ComicItem;