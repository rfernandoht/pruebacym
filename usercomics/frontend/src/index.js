import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import reportWebVitals from './reportWebVitals';

//Componentes
import Navbar from "./components/Navbar/Navbar"
import ComicList from "./components/Comic/ComicList"
import ComicFavoritosList from "./components/Comic/ComicFavoritosList"
import UsuarioForm from "./components/Usuario/UsuarioForm"
import ComicItemDetalle from './components/Comic/ComicItemDetalle';

//styles
import './index.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Navbar />
    <div className='container my-4'>
      <Routes>
        <Route path="/" element={<ComicList />} />
        <Route path="/archivos/:nombreFoto" element={<ComicList />} />
        <Route path="/detalleComic/:id" element={<ComicItemDetalle />} />
        <Route path="/comicFavoritosList/:id" element={<ComicFavoritosList />} />
        <Route path="/comicsfavoritos/:idU/:idC" element={<ComicFavoritosList />} />
        <Route path="/usuarioForm" element={<UsuarioForm />} />
      </Routes>
    </div>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
