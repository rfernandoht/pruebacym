import os
from django.db import models

# Create your models here.


class Usuario(models.Model):
    nombre = models.CharField(
        max_length=120, verbose_name='Nombre del usuario')
    identificacion = models.CharField(
        max_length=12, verbose_name='Identificación del usuario')
    email = models.EmailField(max_length=120, verbose_name='email del usuario')
    password = models.CharField(
        max_length=128, verbose_name='Password de acceso')

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.nombre, self.email)


def path_img_comic(instance, filename):
    #base = "comics"
    folder = "frontend/src/archivos"
    return os.path.join(folder, filename)


class Comic(models.Model):
    nombre = models.CharField(
        max_length=35, verbose_name='Nombre del comic')
    fecha_pub = models.DateField(verbose_name='Fecha de publicación')
    escritor = models.CharField(
        max_length=35, verbose_name='Nombre del escritor')
    dibujante = models.CharField(
        max_length=35, verbose_name='Nombre del dibujante')
    artista_portada = models.CharField(
        max_length=35, verbose_name='Nombre del artista de portada')
    descripcion = models.CharField(
        max_length=35, verbose_name='Descripción del comic')
    imagen = models.ImageField(
        upload_to=path_img_comic, verbose_name='Imagen del comic')

    class Meta:
        verbose_name = "Imágen del Comics"
        verbose_name_plural = "Imagenes de los Comics"
        ordering = ['id']

    def __str__(self):
        return '%s - %s - %s - %s - %s' % (self.id, self.nombre, self.fecha_pub, self.imagen, self.descripcion)


class ComicsFavoritos(models.Model):
    usua = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, verbose_name='Usuario')
    comi = models.ForeignKey(
        Comic, on_delete=models.CASCADE, verbose_name='Comic')

    class Meta:
        verbose_name = "Comic Favorito"
        verbose_name_plural = "Comics Favoritos"
        ordering = ['id']

    def __str__(self):
        return '%s: %s - %s' % (self.id, self.usua, self.comi)
