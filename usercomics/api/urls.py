from flask import Flask
from django.urls import path
from .views import UsuarioView
from .views import ComicView
from .views import ComicsFavoritosView
from .views import ComicImagenView

urlpatterns = [
    path('usuarios/', UsuarioView.as_view(), name='lista_usuarios'),
    path('usuarios/<int:id>', UsuarioView.as_view(), name='procesar_usuario'),
    path('comics/', ComicView.as_view(), name='lista_comics'),
    path('comics/<int:id>', ComicView.as_view(), name='procesar_comic'),
    path('comicsfavoritos/', ComicsFavoritosView.as_view(), name='registra_comic_favorito'),
    path('comicsfavoritos/<int:idU>', ComicsFavoritosView.as_view(), name='lista_comics_favoritos'),
    path('comicsfavoritos/<int:idU>/<int:idC>', ComicsFavoritosView.as_view(), name='procesar_comic_favorito'),
    path('archivos/<nombreFoto>', ComicImagenView.as_view(), name='foto_comics'),
]