from django.contrib import admin
from .models import Usuario
from .models import Comic
from .models import ComicsFavoritos

# Register your models here.

admin.site.register(Usuario)
admin.site.register(Comic)
admin.site.register(ComicsFavoritos)