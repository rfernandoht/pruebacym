from django.db.models import Q
from venv import create
from django.http import JsonResponse
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import json
from .models import ComicsFavoritos
from .models import Usuario
from .models import Comic
import os
from flask import Flask
from flask import send_from_directory

app = Flask(__name__)

CARPETA = os.path.join('archivos')
app.config['CARPETA'] = CARPETA


# Create your views here.

class UsuarioView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if (id > 0):
            usuarios = list(Usuario.objects.filter(id=id).values())
            if len(usuarios) > 0:
                usuario = usuarios[0]
                datos = {'message': "Éxito", 'usuario': usuario}
            else:
                datos = {'message': "Usuario no encontrado"}
            return JsonResponse(datos)
        else:
            usuarios = list(Usuario.objects.values())
            if len(usuarios) > 0:
                datos = {'message': "Éxito", 'usuarios': usuarios}
            else:
                datos = {'message': "Usuarios no encontrados"}
            return JsonResponse(datos)

    def post(self, request):
        print("Respuesta: ", request)
        jd = json.loads(request.body)
        print(jd)
        Usuario.objects.create(
            nombre=jd['nombre'], identificacion=jd['identificacion'], email=jd['email'], password=jd['password'])
        datos = {'message': "Usuario almacenado con Éxito"}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        usuarios = list(Usuario.objects.filter(id=id).values())
        if len(usuarios) > 0:
            usuario = Usuario.objects.get(id=id)
            usuario.nombre = jd['nombre']
            usuario.identificacion = jd['identificacion']
            usuario.email = jd['email']
            usuario.password = jd['password']
            usuario.save()
            datos = {'message': "Usuario actualizado con éxito"}
        else:
            datos = {'message': "Usuario no encontrado"}
        return JsonResponse(datos)

    def delete(self, request, id):
        usuarios = Usuario.objects.filter(id=id).values()
        if len(usuarios) > 0:
            Usuario.objects.filter(id=id).delete()
            datos = {'message': "Usuario eliminado"}
        else:
            datos = {'message': "Usuario no encontrado"}
        return JsonResponse(datos)


class ComicView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if (id > 0):
            comics = list(Comic.objects.filter(id=id).values())
            if len(comics) > 0:
                comic = comics[0]
                datos = {'message': "Éxito", 'comic': comic}
            else:
                datos = {'message': "Comic no encontrado"}
            return JsonResponse(datos)
        else:
            comics = list(Comic.objects.values())
            if len(comics) > 0:
                datos = {'message': "Éxito", 'comics': comics}
            else:
                datos = {'message': "Comics no encontrados"}
            return JsonResponse(datos)

    def post(self, request):
        print("Respuesta: ", request)
        jd = json.loads(request.body)
        print(jd)
        Comic.objects.create(
            nombre=jd['nombre'], fecha_pub=jd['fecha_pub'], escritor=jd['escritor'], dibujante=jd['dibujante'], artista_portada=jd['artista_portada'], descripcion=jd['descripcion'], imagen=jd['imagen'])
        datos = {'message': "Comic almacenado con Éxito"}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        comics = Comic.objects.filter(id=id)
        if len(comics) > 0:
            comics[0].nombre = jd['nombre']
            comics[0].fecha_pub = jd['fecha_pub']
            comics[0].escritor = jd['escritor']
            comics[0].dibujante = jd['dibujante']
            comics[0].artista_portada = jd['artista_portada']
            comics[0].descripcion = jd['descripcion']
            comics[0].imagen = jd['imagen']
            comics[0].save()
            datos = {'message': "Comic actualizado con éxito"}
        else:
            datos = {'message': "Comic no encontrado"}
        return JsonResponse(datos)

    def delete(self, request, id):
        comics = Comic.objects.filter(id=id).values()
        if len(comics) > 0:
            Comic.objects.filter(id=id).delete()
            datos = {'message': "Comic eliminado"}
        else:
            datos = {'message': "Comic no encontrado"}
        return JsonResponse(datos)


class ComicsFavoritosView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, idU):
        if idU > 0:
            comicsFav = list(ComicsFavoritos.objects.filter(usua=idU).values())
            if len(comicsFav) > 0:
                datos = {'message': "Éxito", 'comicsFav': comicsFav}
            else:
                datos = {'message': "Comics Favoritos no encontrados"}
        else:
            datos = {'message': "Debe especificar el usuario"}
        return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        comicsFav = list(ComicsFavoritos.objects.filter(
            usua=jd['usua'], comi=jd['comi']).values())
        if len(comicsFav) > 0:
            datos = {'message': "El Comic ya existe en los favoritos del usuario"}
        else:
            usuario = Usuario.objects.get(id=jd['usua'])
            comiT = Comic.objects.get(id=jd['comi'])
            ComicsFavoritos.objects.create(
                usua=usuario,
                comi=comiT
            )
            datos = {'message': "Comic asignado a los favoritos del usuario"}
        return JsonResponse(datos)

    def delete(self, request, idU, idC):
        print(idU, idC)
        comics = ComicsFavoritos.objects.filter(usua=idU, comi=idC).values()
        if len(comics) > 0:
            ComicsFavoritos.objects.filter(usua=idU, comi=idC).delete()
            datos = {'message': "Comic Favorito eliminado"}
        else:
            datos = {'message': "Comic Favorito no encontrado"}
        return JsonResponse(datos)


class ComicImagenView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, nombreFoto):
        print(app.config['CARPETA'])
        print(nombreFoto)
        with app.app_context():
            yield
        return send_from_directory(app.config['CARPETA'], nombreFoto)
