# Pruebacym

Proyecto creado para el almacenamiento de la prueba técnica de la empresa C&M

# Prueba Técnica

## FrontEnd: React

## Backend: Django Rest Framework - Python

## Estilos: Bootstrap

## Entorno virtual
Para configurar el ambiente, se creó un entorno virtual independiente, ubicado en la carpeta: `prueba` 

Para iniciar el entorno se debe ejecutar: `.\prueba\Scripts\activate`

## Backend

Se debe ubicar en la carpeta: `usercomics`

Para iniciar el servidor: `python manage.py runserver`

Para acceder al backend y desplegar en el navegador: [http://localhost:8000](http://localhost:8000)

## Se habilita el entorno de administración
Al cual se puede accederr por la ruta: en el navegador: [http://localhost:8000/admin](http://localhost:8000/admin)

## FrontEnd

Se debe ubicar en la carpeta: usercomics/frontend:

Para iniciar el servidor: `npm start`

Para acceder al Frontend y desplegar en el navegador: [http://localhost:3000](http://localhost:3000)